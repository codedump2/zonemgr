#!/bin/bash

set -euo pipefail

: ${CERT_BASE:=/etc/ssl/custom}
: ${ACME_HOME:=/home/acme}
: ${ZONEDIR:=/etc/nsd/zonefiles}
: ${NSD_HOST:=172.16.128.53}

# both $ZONE and $DOMAIN are the main domain, but
# $ZONE is used for locating the NSD data only,
# while $DOMAIN is the directory name of the certificate.

## try to determine $ZONE/$DOMAIN from env var, or from the
## first -d parameter guess the domain name, we'll use it as
## a ZONEFILE substitute.
ZONE=${ZONE:="auto"}
DOMAIN=${DOMAIN:="auto"}

POS=1
NEXT=no
for var in "$@"; do
    POS=$[POS+1]
    if [ "$var" == "-d" ]; then
        NEXT="yes"
        continue
    fi

    if [ "$NEXT" == yes ]; then
        [ "$ZONE" == "auto" ] && ZONE="$var"
        [ "$DOMAIN" == "auto" ] && DOMAIN="$var"
        break
    fi
done


echo "Zone: $ZONE"
echo "Domain: $ZONE"

## If we have a zonefile, we initialize a working dns_nsd environment.
ZONEFILE="$ZONEDIR/$ZONE"
if [ -f "$ZONEFILE" ]; then
    echo "Zonefile: $ZONEFILE"
    echo "Preparing NSD control variables on NSD host $NSD_HOST"
    export Nsd_ZoneFile="$ZONEFILE"
    export Nsd_Command="nsd-control -s $NSD_HOST reload $ZONE"
else
    echo "No zonefile: $ZONEFILE"
    # this is ok, some commands don't need one; those that do (e.g. --issue)
    # will fail.
fi

mkdir -p "$CERT_BASE/$DOMAIN"

acme.sh \
     --home           "$ACME_HOME" \
     --dns            dns_nsd \
     --cert-file      "$CERT_BASE/$DOMAIN/cert.pem" \
     --key-file       "$CERT_BASE/$DOMAIN/privkey.pem" \
     --ca-file        "$CERT_BASE/$DOMAIN/ca.pem" \
     --fullchain-file "$CERT_BASE/$DOMAIN/fullchain.pem" \
     --reloadcmd      "cat $CERT_BASE/$DOMAIN/privkey.pem $CERT_BASE/$DOMAIN/cert.pem >> $CERT_BASE/$DOMAIN/key+cert.pem && chmod 0600 $CERT_BASE/$DOMAIN/key+cert.pem" \
     $@
