#!/bin/bash

#
# Creates an initial certificate nest for domain ($1) and subdomains ($2, $3, ...)
# The idea is to use letsencrypt's certbot only for key signing, and do
# everything else (key creation etc) by hand. This is useful if you want
# to retain control over your keys for more than 3 months (cerbot's maximum
# signing period), necessary e.g. for DANE support.
#

DOMAIN=$1
[ "x$DOMAIN" == "x" ] && echo "Usage: $0 domain.org [sub1 [sub2 [...]]]" && exit

# This will shift the arguments list, skipping $1.
# The next argument (subdomain list) is now available as $1, $2, ...
shift

# By default, the domain nest will be prepared under $BASE/$DOMAIN.
# change the base dir to something sensible for production, e.g.
# /etc/ssl/schaeffer/ ...
#BASE=`dirname $0`
: ${CERT_BASE:=/etc/ssl/custom}

DOMDIR=$BASE/$DOMAIN

# this file is needed by cert-sign.sh -- do not change the location
CST=$DOMDIR/request.csr

echo "Preparing nest $DOMDIR"
mkdir -p $DOMDIR

#
# Create private & public key
#
echo "Creating keys"
openssl genpkey -algorithm RSA -out $DOMDIR/privkey.pem -pkeyopt rsa_keygen_bits:4096
openssl rsa -pubout -in $DOMDIR/privkey.pem -out $DOMDIR/pubkey.pem
chown root.ssl-cert $DOMDIR/privkey.pem
chmod 0640 $DOMDIR/privkey.pem

#
# Create CSR (certificate signing request)
#
echo "Creating configuration for host identity"

## Prepare config file -- only add req_ext if subdomains exist
echo "  Domain: $DOMAIN"
if [ "x$1" == "x" ]; then
    cat > $DOMDIR/request.conf << EOF
    [req]
    prompt = no
    distinguished_name = req_dn

    [req_dn]
    commonName = $DOMAIN

EOF

else
    cat > $DOMDIR/request.conf << EOF
    [req]
    prompt = no
    distinguished_name = req_dn
    req_extensions = req_ext

    [req_dn]
    commonName = $DOMAIN

    [req_ext]
    subjectAltName=@alt_names

    [alt_names]
EOF

    ## add subdomains
    subcnt=1
    while [ "x$1" != "x" ]; do
        sub=$1
        echo "  Adding: $sub"
        echo "    DNS.$subcnt=$sub.$DOMAIN" >> $DOMDIR/request.conf
        shift
	subcnt=$[$subcnt+1]
    done
fi


# Creating the actual CSR
echo "Creating signing request"
openssl req -new -key $DOMDIR/privkey.pem -config $DOMDIR/request.conf -out $DOMDIR/request.csr
openssl req -in $DOMDIR/request.csr -out $DOMDIR/request.der -outform DER

echo "Creating a self-signed version of the certificate (because it's fun)"
openssl x509 -req -in $DOMDIR/request.csr -signkey $DOMDIR/privkey.pem -out $DOMDIR/selfsign.crt

if [ -f "$DOMDIR/request.csr" ]; then
    echo -e "To sign your request by Let's Encrypt, type:\n\n\t`dirname $0`/cert-sign.sh $DOMAIN\n"
else
    echo "Failed."
fi


