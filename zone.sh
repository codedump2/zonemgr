#!/bin/bash

set -euo pipefail

domain=$1
HERE=`dirname $0`

function usage_and_exit {
    echo "Usage: $0 domain.org"
    exit
}

[ "x$domain" = "x" ] && usage_and_exit


#
# This is where we're going to actually store the keys and handle the zonefiles.
#
: ${ZONEDIR:=/etc/nsd/zonefiles}

## define this if you want to automatically reload the zone after signing
: ${NSD_CONTROL:="nsd-control"}

#
# Checks whether keys for domain $1 already exist. If yes, complains and exits.
#
function check_keys {
    DOM=$1

    OLDKEYS=`ls $ZONEDIR/K$DOM.* `
    if [ "x$OLDKEYS" != "x" ]; then
	echo Old keys found:
	for k in $OLDKEYS; do
	    echo \ \ $k
	done
	echo You need to delete those first.
    fi
}

#
# Generates new keys for zone $1, saves the file names in $KSK and $ZSK.
#
function gen_keys {
    pushd

    DOM=$1

    ZSK=`ldns-keygen    -a RSASHA256 -b 2048 $DOM`
    KSK=`ldns-keygen -k -a RSASHA256 -b 2048 $DOM`
    rm -rf $ZSK.ds $KSK.ds

    echo New keys created:
    echo \ \ $ZSK
    echo \ \ $KSK

    popd
}

#
# Reads the keys from the $ZONEDIR/$1, returns the key names in variables $KSK and $ZSK
#
function get_keys {
    DOM=$1

    ZSK=$(basename $(grep -r "`grep '(ksk)' $ZONEDIR/$DOM.signed | cut -f3-10`" $ZONEDIR/K$DOM.*.key | cut -d':' -f1) .key)
    KSK=$(basename $(grep -r "`grep '(zsk)' $ZONEDIR/$DOM.signed | cut -f3-10`" $ZONEDIR/K$DOM.*.key | cut -d':' -f1) .key)
    echo Retrieved keys:
    echo \ \ $ZSK
    echo \ \ $KSK

}

#
# Signs zone $DOM using $KSK and $ZSK keys
#
function sign_zone()
{
    DOM=$1

    SALT=`head -c 16 /dev/urandom | sha256sum | cut  -b 1-16`
    echo Salt: $SALT


    pushd $ZONEDIR > /dev/null

    echo Signing zone: $DOM
    ldns-signzone -e $[`date +%Y`+50]`date +%m%d` -n -p -s "$SALT" $ZONEDIR/$DOM $ZSK $KSK

    popd
}


#
# Generates DS records and checks them against the DNS published ones
#
function check_ds()
{
    DOM=$1

    echo "DS records:"
    DS1=`ldns-key2ds -n -1 $ZONEDIR/$DOM.signed`
    DS2=`ldns-key2ds -n -2 $ZONEDIR/$DOM.signed`
    echo $DS1
    echo $DS2
 
    # Comparing DS records on-line and generated to decide whether
    # they need to be updated. That's actually tricky, because
    # the formats differ slightly.

    # dig makes all hex letters lower case
    FOO=`dig DS $DOM. +short +norec`
    CMP_ONLINE=`echo $FOO | cut -d\  -f 1-3``echo $FOO | cut -d\  -f4`
    
    # ldns separates the fingerprint and the salt into several parts and makes small hex letters
    CMP_NEWDS2=`echo $DS2 | cut -f5-8 | tr [:lower:] [:upper:]`

    if [ "$CMP_ONLINE" != "$CMP_NEWDS2" ]; then
	echo
	echo    " *** WARNING! ***"
	echo
	echo "A DS record for $DOM is already on-line but doesn't match the current"
	echo "signature anymore. It needs to be updated (check with your DNS upstream)!"
	echo "  $CMP_ONLINE  ; (currently on-line)"
	echo "  $CMP_NEWDS2  ; (newly created)"
    fi
}

function reload_zone {
    DOM=$1
    echo Reloading nsd for: $DOM
    echo Reload command: \'$NSD_CONTROL\'
    $NSD_CONTROL reload $DOM
    $NSD_CONTROL notify $DOM
}


case `basename $0` in
    zone-init.sh)
	echo "Initializing keys: $domain"
	check_keys $domain
	gen_keys $domain
	sign_zone $domain
	check_ds $domain
	;;
    
    zone-sign.sh)
	echo "(Re-)Signing zone: $domain"
	get_keys $domain
	sign_zone $domain
	reload_zone $domain
	;;

    *) usage_and_exit ;;
esac
