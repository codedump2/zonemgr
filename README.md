# ZoneMgr - Managing zones and certificates

This is a number of scripts for managing zones and certificates in a
setup based on [acme.sh](https://github.com/acmesh-official/acme.sh)
and [nsd](https://www.nlnetlabs.nl/projects/nsd/).

Original motivation was to create fixed certificates and signing-only 
requests to [Let's Encrypt](https://letsencrypt.org/). This was
necessary / useful for DNSSEC / DANE setups.

Now, DANE is dead, and DNSSEC is... well. So there's no need anymore
for this kind of setup. Still, the scripts are here, and a few more
helpers for deploying an NSD / acme.sh / LetsEncrypt setup based
on Podman containers.

## Persistent certificates for DNSSEC/DANE

### Generating persistent certificates for LetsEncrypt

This being an obsolete way of doing things, this is just a quick
heads-up:

Use ```cert-init.sh``` / ```cert-sign.sh``` to create persistent certificates
(i.e. keys that don't change), and custom CSRs to sign those with with Letsencrypt:
```
    $ export CERT_BASE=/etc/ssl/my-cert
    $ cert-init.sh my-domain.com www.my-domain.com
    $ cert-sign.sh my-domain.com
```

Find your certificates under `/etc/ssl/my-cert`, including a number of derivates
(i.e. PEM format certificates, full-chains etc).

To renew the signature, just repeat the signing call periodically, e.g. every 60 days:
```
    $ export CERT_BASE=/etc/ssl/my-cert
    $ cert-sign.sh my-domain.com
```

### Sigining persistent certificates for DNSSEC/DANE

The problem with DANE is that the certificates you're using are pinned down
in your own DNS entry, which needs to be secured by DNSSEC. This is obsolete
news, here's a quick-n-dirty run just for history's sake. No explanations:
```
    $ export ZONEDIR=/etc/nsd/zonefiles
    $ export NSD_CONTROL="nsd-control -s 127.0.0.1"
    $ ./zone-init.sh my-domain.com
    $ ./zone-sign.sh my-domain.com
```

You may also need to follow some instructions on the screen about entries
to be made to your zonefile. Or something.

The point is that you *don't* need to repeat the procedure when you re-sign
the certificates using LetsEncrypt, because the keys themselves never change.


## Using with NSD deployment inside Podman

### Custm NSD Reload

The trick with Podman (or Docker) is that the `nsd-control` command needs
the specific address of the NSD pod using the `-s` parameter. For instance:
```
    $ nsd-control -s 192.168.1.1 status
    version: 4.1.26
    verbosity: 2
    ratelimit: 200
```

How to set up NSD for remote control is best explained by the NSD documentation
itself -- it usually involves creating a symmetric key to be shared between
the `nsd` server (i.e. the pod) and the `nsd-control` client (e.g. the host).


### Using ACME with NSD-based DNS challenge

The script `acme-nsd-run` runs `acme.sh` with `--dns dns_nsd`, i.e. using the NSD
specific DNS backend for a DNS-based LetsEncrypt challenge. Is essentially a wrapper
for setting all the right variables, then calling `acme.sh`. This is how to use it:
```
    $ export ZONEDIR=/etc/nsd/zonefiles
    $ export NSD_HOST=127.0.0.1
    $ ./acme-nsd-run --issue -d my-domain.com
```

The main thing the script does is to assume that the NSD zonefile for the domain
you're trying to sign is named like the main domain itself (e.g. "my-domain.com"),
and to extract that name from the first "-d" parameter.

The full path of the zonefile (for instance `$ZONEDIR/my-domain.com`) is exported
into the `Nsd_ZoneFile` variable, an appropriate entry is made for `Nsd_Command`,
and `acme.sh` is called using the `--dns dns_nsd` challenge.

Behind the curtain, the script also sets some more command line arguments, most
notably `--home $ACME_HOME` and various `--cert-file` and `--relaodcmd` trickeries
to generate more useful certificate copies in the `$CERT_BASE/$ZONE` directory
(e.g. `/etc/ssl/local`, for use with various services like Apache).

Renewing certificates works as expected, but you may need to mind the
`--home` parameter yourself if you're using a non-standard location for
the ACME storage dir:
```
    $ acme.sh --home /home/acme --renew -d my-domain.com
```
